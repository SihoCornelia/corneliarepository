package com.haynespro.test;

import java.time.LocalDate;

public class Season {
    private enum SEASON {SPRING, SUMMER, AUTUMN, WINTER}
    private static LocalDate today = LocalDate.now();

    public String get() {
        return getSeasonByDate(today.getMonthValue());
    }

    public String getPrevious() {
        return getSeasonByDate((today.getMonthValue() + 9)%12);
    }

    public String getNext() {
        return getSeasonByDate((today.getMonthValue() + 3)%12);
    }

    private String getSeasonByDate(int monthValue) {
        switch (monthValue) {
            case 12:
            case 0:
            case 1:
            case 2:
                return SEASON.WINTER.toString();
            case 3:
            case 4:
            case 5:
                return SEASON.SPRING.toString();
            case 6:
            case 7:
            case 8:
                return SEASON.SUMMER.toString();
            case 9:
            case 10:
            case 11:
                return SEASON.AUTUMN.toString();
        }
        return "";
    }


}
